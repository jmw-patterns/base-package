<?php

namespace Tfive\Patterns\Abstracts;

use Tfive\Patterns\Traits\RepeaterTrait;

/**
 * Class AbstractRepeater
 * @package ThreeFiveACF\Abstracts
 */
abstract class AbstractRepeater extends AbstractPattern {
	use RepeaterTrait;

	/**
	 * @var array $items
	 */
	protected $items = [ ];
}
